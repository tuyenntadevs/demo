package com.javatpoint;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.AssertJUnit;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

public class SeleniumTest {
	private WebDriver driver;

	@Test
	public void testEasy() {
		driver.get("http://demo.guru99.com/test/guru99home/");
		String title = driver.getTitle();
		AssertJUnit.assertTrue(title.contains("Demo Guru99 Page"));
	}

	@BeforeTest
	public void beforeTest() {
		if (System.getProperty("os.name").contains("Windows")){
			System.setProperty("webdriver.chrome.driver", "src/main/resources/chromedriver.exe");
			System.out.println("Test on: " + System.getProperty("os.name") + " system");
			driver = new ChromeDriver();
		} else {
			System.setProperty("webdriver.firefox.driver", "src/main/resources/geckodriver_linux");
			System.out.println("Test on: " + System.getProperty("os.name") + " system");
			driver = new FirefoxDriver();
		}
		
	}

	@AfterTest
	public void afterTest() {
		driver.quit();
	}
}
